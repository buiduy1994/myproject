from django.shortcuts import render
from .models import Post
from django.http import HttpResponse

# Create your views here.
# posts = [
#     {
#         'author': 'Duy',
#         'title' : 'Blog post1 title',
#         'content':'ghi thế nào thì ghi',
#         'date_posted':'Jan 10, 2019',
#     },
#     {
#         'author': 'B',
#         'title' : 'Blog post2 title',
#         'content':'ghi thế nào thì ghi',
#         'date_posted':'Jan 10, 2019',
#     }
# ]
def home(request):
    context = {
        'posts': Post.objects.all()
    }
    return render(request,'blog/home.html',context)

def about(request):
    return render(request,'blog/about.html',{'title' : 'About'})